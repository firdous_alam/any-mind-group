import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.6.1"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("jvm") version "1.6.0"
    kotlin("plugin.spring") version "1.6.0"
    kotlin("kapt") version "1.6.0"

    id("com.github.johnrengelman.processes") version "0.5.0"
    id("org.springdoc.openapi-gradle-plugin") version "1.3.0"
    id("org.hidetake.swagger.generator") version "2.18.2"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.slf4j:slf4j-api:1.7.30")
    implementation("org.springdoc:springdoc-openapi-ui:1.5.9")
    implementation ("io.swagger.core.v3:swagger-annotations:2.1.10")
    swaggerCodegen ("io.swagger.codegen.v3:swagger-codegen-cli:3.0.27")
    implementation("org.mapstruct:mapstruct:1.4.2.Final")
    kapt("org.mapstruct:mapstruct-processor:1.4.2.Final")
    implementation("com.oracle.database.jdbc:ojdbc8")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

openApi {
    apiDocsUrl.set("http://localhost:8090/anymindgroup/v3/api-docs")
    outputDir.set(file("$projectDir/src/main/resources"))
    outputFileName.set("api-spec.json")
    waitTimeInSeconds.set(120)
}

swaggerSources {
    create("api-client-code") {
        setInputFile(file("$projectDir/src/main/resources/api-spec.json"))
        code(
            closureOf<org.hidetake.gradle.swagger.generator.GenerateSwaggerCode> {
                language = "python"
                configFile = file("$projectDir/src/main/resources/api-config.json")
                outputDir = file(
                    "$rootDir/modules/api-client"
                )
            }
        )
    }
}

tasks.named("build") { dependsOn("generateSwaggerCode") }

springBoot {
    mainClass.set("alam.firdous.anymindgroup.AnyMindGroupApplicationKt")
}

group = "alam.firdous"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11
