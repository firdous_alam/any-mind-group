package alam.firdous.anymindgroup.mapper

import alam.firdous.anymindgroup.dto.UserDto
import alam.firdous.anymindgroup.entity.User
import org.mapstruct.InjectionStrategy
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.FIELD)
interface UserConverter {
    fun convertToUserDto(userEntity: User): UserDto
    fun convertToUserEntity(userDto: UserDto): User
}
