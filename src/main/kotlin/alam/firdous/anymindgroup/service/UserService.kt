package alam.firdous.anymindgroup.service

import alam.firdous.anymindgroup.entity.User
import alam.firdous.anymindgroup.repository.UserRepository
import java.util.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class UserService {

    @Autowired
    lateinit var userRepository: UserRepository

    @Transactional(readOnly = true)
    fun isAllReadyRegistered(userName: String): Boolean {
        val user = userRepository.findByUserName(userName)
        return user.isPresent
    }

    @Transactional(readOnly = true)
    fun getUserByUserName(userName: String): Optional<User> {
        return userRepository.findByUserName(userName)
    }

    @Transactional(readOnly = true)
    fun getUsers(): List<User> {
        return userRepository.findAll()
    }

    @Transactional
    fun save(user: User): User{
        return userRepository.save(user)
    }
}
