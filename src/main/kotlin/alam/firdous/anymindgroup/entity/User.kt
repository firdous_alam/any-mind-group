package alam.firdous.anymindgroup.entity

import alam.firdous.anymindgroup.converter.GenderConverter
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "fa_user")
class User {

    @Id
    @Column(name = "user_name")
    lateinit var userName: String

    @Column(name = "user_password")
    lateinit var userPassword: String

    @Column(name = "user_id")
    var userId: Int = 0

    @Column(name = "first_name")
    lateinit var firstName: String

    @Column(name = "last_name")
    var lastName: String?= null

    @Column(name = "contact")
    lateinit var contactNumber: String

    @Column(name = "gender")
    @Convert(converter = GenderConverter::class)
    var gender: Gender?= null
}
