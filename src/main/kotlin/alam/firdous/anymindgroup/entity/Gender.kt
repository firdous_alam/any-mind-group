package alam.firdous.anymindgroup.entity

enum class Gender(val dValue: String) {
    Male("Male"), Female("Female");

    companion object {
        fun fromStringOrNull(value: String?): Gender? {
            return values().firstOrNull { it.dValue == value }
        }
    }
}
