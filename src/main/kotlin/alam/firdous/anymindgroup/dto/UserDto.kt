package alam.firdous.anymindgroup.dto

import alam.firdous.anymindgroup.entity.Gender

class UserDto {

    lateinit var userName: String

    lateinit var userPassword: String

    var userId: Int = 0

    lateinit var firstName: String

    var lastName: String?= null

    lateinit var contactNumber: String

    var gender: Gender?= null
}
