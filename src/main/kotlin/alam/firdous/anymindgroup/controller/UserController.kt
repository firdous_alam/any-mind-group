package alam.firdous.anymindgroup.controller

import alam.firdous.anymindgroup.dto.UserDto
import alam.firdous.anymindgroup.mapper.UserConverter
import alam.firdous.anymindgroup.response.Response
import alam.firdous.anymindgroup.service.UserService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder

@RestController
@RequestMapping("/api/v1")
class UserController {

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var userConverter: UserConverter

    @GetMapping("user/registered/{userName}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun isAllReadyRegistered(@PathVariable("userName") userName: String): Boolean {
        return userService.isAllReadyRegistered(userName)
    }

    @GetMapping("user/{userName}", produces = [MediaType.APPLICATION_JSON_VALUE])
    @ExceptionHandler(Exception::class)
    fun getUserByUserName(@PathVariable("userName") userName: String): ResponseEntity<UserDto> {
        val user = userService.getUserByUserName(userName)
        return ResponseEntity.ok(userConverter.convertToUserDto(user.get()))
    }

    @GetMapping("users", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getUsersList(): ResponseEntity<Any> {
        try {
            val usersList = userService.getUsers()
            return if (usersList.isNotEmpty()) {
                val usersDtoList = usersList.map { userConverter.convertToUserDto(it) }
                ResponseEntity(usersDtoList, HttpStatus.OK)
            } else {
                val body = Response(HttpStatus.BAD_REQUEST, "Empty user list").build()
                ResponseEntity(body, HttpStatus.BAD_REQUEST)
            }
        } catch (e: Exception) {
            LOGGER.error("Exception occurred while getting the users list", e)
            throw e
        }
    }

    @PostMapping("user/register", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun registerUser(@RequestBody userDto: UserDto): ResponseEntity<Any> {
        return try {
            if (!isAllReadyRegistered(userDto.userName)) {
                val user = userConverter.convertToUserEntity(userDto)
                userService.save(user)
                val location = ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{userName}").buildAndExpand(user.userName).toUri()
                ResponseEntity.created(location).build()
            } else {
                val body = Response(HttpStatus.BAD_REQUEST, "User already registered").build()
                ResponseEntity(body, HttpStatus.BAD_REQUEST)
            }
        } catch (e: Exception) {
            LOGGER.error("Exception occurred while registering the user", e)
            throw e
        }
    }

    companion object { val LOGGER: Logger = LoggerFactory.getLogger(this::class.java) }
}
