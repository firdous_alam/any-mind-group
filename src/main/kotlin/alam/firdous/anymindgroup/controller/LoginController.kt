package alam.firdous.anymindgroup.controller

import alam.firdous.anymindgroup.dto.UserDto
import alam.firdous.anymindgroup.entity.User
import alam.firdous.anymindgroup.exception.UserNameNotFoundException
import alam.firdous.anymindgroup.mapper.UserConverter
import alam.firdous.anymindgroup.response.Response
import alam.firdous.anymindgroup.service.UserService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder

@RestController
@RequestMapping("/api/v1")
class LoginController {

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var userConverter: UserConverter

    @PostMapping("user/login", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun registerUser(@RequestParam(name = "userName") userName: String)
    : ResponseEntity<UserDto> {
        try {
            val user = userService.getUserByUserName(userName)
            if (!user.isPresent) {
                throw NoSuchElementException("$userName not found")
            } else {
                return ResponseEntity.ok(userConverter.convertToUserDto(user.get()))
            }
        } catch (e: Exception) {
            LOGGER.error("Exception occurred: ", e)
            throw e
        }
    }

    companion object { val LOGGER: Logger = LoggerFactory.getLogger(this::class.java) }
}
