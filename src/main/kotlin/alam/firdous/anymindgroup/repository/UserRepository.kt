package alam.firdous.anymindgroup.repository

import alam.firdous.anymindgroup.entity.User
import java.util.*
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : JpaRepository<User, String> {
    fun findByUserName(userName: String): Optional<User>
}
