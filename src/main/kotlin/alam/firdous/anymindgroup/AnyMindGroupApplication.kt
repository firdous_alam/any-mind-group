package alam.firdous.anymindgroup

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AnyMindGroupApplication
fun main(args: Array<String>) {
	runApplication<AnyMindGroupApplication>(*args)
}
