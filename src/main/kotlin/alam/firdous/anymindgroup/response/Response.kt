package alam.firdous.anymindgroup.response

import org.springframework.http.HttpStatus
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.time.LocalDateTime

class Response(private val error: HttpStatus, private val message: String) {

    private val body: MutableMap<String, Any> = LinkedHashMap()

    fun build(): MutableMap<String, Any> {
        body["error"] = error
        body["message"] = message
        body["status"] = error.value()
        body["timestamp"] = LocalDateTime.now()
        body["path"] = ServletUriComponentsBuilder.fromCurrentRequestUri().build().path.toString()
        return body
    }
}
