package alam.firdous.anymindgroup.converter

import alam.firdous.anymindgroup.entity.Gender
import javax.persistence.AttributeConverter

class GenderConverter : AttributeConverter<Gender?, String?> {
    override fun convertToDatabaseColumn(attribute: Gender?): String? {
        return attribute?.name
    }

    override fun convertToEntityAttribute(dbData: String?): Gender? {
        return Gender.fromStringOrNull(dbData)
    }
}
