package alam.firdous.anymindgroup.exception

open class UserNameNotFoundException : Exception {

    constructor(message: String) : super(message)

    constructor(message: String, e: Exception) : super(message, e)
}
