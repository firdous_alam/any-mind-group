package alam.firdous.anymindgroup.exception

import alam.firdous.anymindgroup.response.Response
import com.fasterxml.jackson.databind.JsonMappingException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ExceptionHandler {

    @ExceptionHandler(HttpMessageNotReadableException::class)
    fun jsonParsingException(e: HttpMessageNotReadableException): ResponseEntity<MutableMap<String, Any>> {
        val jme: JsonMappingException = e.cause as JsonMappingException
        return ResponseEntity(Response(HttpStatus.BAD_REQUEST, jme.path[0].fieldName.toString() + " invalid")
            .build(), HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(Exception::class)
    fun genericException(e: Exception): ResponseEntity<MutableMap<String, Any>> {
        val httpStatus: HttpStatus
        val message: String

        when (e) {
            is NoSuchElementException -> {
                httpStatus = HttpStatus.NOT_FOUND
                message = e.message!!
            }
            else -> {
                httpStatus = HttpStatus.BAD_REQUEST
                message = e.message!!
            }
        }

        return ResponseEntity(Response(httpStatus, message).build(), httpStatus)
    }
}
